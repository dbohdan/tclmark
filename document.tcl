#! /usr/bin/env tclsh

source [file dirname [info script]]/tclmark.tcl
set tclmark::debug true

lassign $argv n
if {$n eq {}} { set n 1 }

puts [tclmark::render [string repeat {

[h1 twtxt.tcl]

This repository hosts tools for the [link https://wiki.tcl-lang.org/page/twtxt twtxt] feed format.

It contains two applications:
[* {
    A basic cross-platform GUI twtxt feed reader ([link app/timeline.tcl [tt app/timeline.tcl]]);
} {
    A command line utility for extracting all mentions from a twtxt feed ([link app/mentions.tcl [tt app/mentions.tcl]]);
} {
    A command line utility for converting an RSS feed to a twtxt feed ([link app/rss2twtxt.tcl [tt app/rss2twtxt.tcl]]).
}]

It also includes Tcl libraries for
[* {
    Parsing a twtxt feed ([link lib/twtxt/twtxt.tcl [tt lib/twtxt/twtxt.tcl]]);
} {
    Displaying emoji in a Tk text widget in Tcl/Tk builds limited to the basic multilingual plane ([link lib/emoji-hack/emoji-hack.tcl [tt lib/emoji-hack/emoji-hack.tcl]]);
} {
  Reading files in a uniform manner form the local filesystem, HTTP(S), Gopher, and Gemini. ([link lib/read-url/read-url.tcl [tt lib/read-url/read-url.tcl]]).
}]


[h2 { Feed reader screenshot }]

[img screenshot.png {
    A screenshot of app/timeline.tcl showing some bots
}]


[h2 Dependencies]

[h3 Programs]

[h4 { Feed reader [tt app/timeline.tcl] }]

[* {
    Tcl 8.6 (preferably 8.6.10 or later
} {
    Tk
} {
    The SQLite3 extension for Tcl
} {
    [link https://wiki.tcl-lang.org/page/tls TclTLS] (*nix) or
    [link https://wiki.tcl-lang.org/page/TWAPI TWAPI] (Windows)
}]

On Debian/Ubuntu you can install all of the above with the command

[code {
sudo apt install libsqlite3-tcl tcl tcl-tls tk
}]

[h4 [tt app/mentions.tcl]]

[* {
    Tcl 8.6 or later
} {
    TclTLS or TWAPI
}]

[h4 [tt app/rss2twtxt.tcl]]

[* {
    Tcl 8.6 or later
} {
    Tcllib 1.15 or later
} {
  TclTLS or TWAPI
} {
    [link https://wiki.tcl-lang.org/page/tDOM tDOM]
}]

[code {
sudo apt install tcl tcllib tcl-tls tdom
}]

[h3 Libraries]

[h4 [tt lib/twtxt/twtxt.tcl]]

[* {
    Tcl 8.5 or later.  Jim Tcl works conditionally and may parse timestamps
    incorrectly.
}]

[h4 [tt lib/emoji-hack/emoji-hack.tcl]]

[* { Tcl 8.6.10 or later }]

[h4 [tt lib/read-url/read-url.tcl]]

[* {
    Tcl 8.6 or later
} {
    TclTLS or TWAPI for accessing HTTPS and Gemini
}]


[h2 License]

MIT.  See the file [link LICENSE].

[link vendor/twemoji]: Copyright 2019 Twitter, Inc and other contributors.  CC-BY 4.0 license.

} $n]]
