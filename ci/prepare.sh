#! /bin/sh

set -e

cd "$(dirname "$(readlink -f "$0")")" || exit
echo "running $(basename "$0") in $(pwd)"

wget -O tclkit-8.6.10 http://kitcreator.rkeene.org/kits/1027aea8fcb581ff672bf7800aabc268f50c6150/tclkit

echo 'a6f52deb549d019b10dd21ed2327c3359656c891bdf63b4ea4cc853ad4c065d1  tclkit-8.6.10' >> checksum
sha256sum -c checksum && rm checksum

chmod +x tclkit*
