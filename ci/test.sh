#! /bin/sh

set -e

cd "$(dirname "$(readlink -f "$0")")" || exit
echo "running $(basename "$0") in $(pwd)"

run() {
    printf "\n===== %s\n" "$*"
    "$@"
}

run ./tclkit-8.6.10 ../tclmark.tcl
