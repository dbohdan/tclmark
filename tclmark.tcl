#! /usr/bin/env tclsh
# Copyright (c) 2021 D. Bohdan.
# License: MIT.

namespace eval tclmark {
    variable debug false
    variable interp
    variable renders 0
    variable token
    variable version 0.1.0

    namespace eval cmd {
        namespace path [namespace parent]
    }
}


proc tclmark::render {content {variables {}}} {
    variable interp
    variable renders
    variable token

    incr renders
    set interp tclmark-$renders

    set token [token $renders $content]

    try {
        interp create -safe $interp

        foreach command [interp eval $interp {info commands}] {
            $interp hide $command
        }

        foreach command [info commands cmd::*] {
            $interp alias \
                [namespace tail $command] \
                $command \
        }

        join [msubst $content flatten list] {}
    } finally {
        catch {
            interp delete $interp
        }
    }
}


proc tclmark::token {n text} {
    # The token should be unguessable by the user.
    format %d-%x-%x \
        [expr { $n % 1000000 } ] \
        [expr { int(0xFFFFFFFF * rand()) } ] \
        [zlib crc32 $text] \
}


proc tclmark::msubst {
    content
    {elementScript lindex}
    {treeScript {node tree}}
} {
    variable interp
    variable token

    # Pass through already processed content marked with the
    # token.
    set tokenLen [string length $token]
    if {[string range $content 0 $tokenLen-1] eq $token} {
        return $content
    }

    {*}$treeScript {*}[lmap segment [segment $content] {
        {*}$elementScript [if {[string index $segment 0] eq {[}} {
            $interp invokehidden \
                subst -nobackslashes -novariables $segment
        } else {
            node text $segment
        }]
    }]
}


proc tclmark::segment text {
    set opening \[

    set matches [regexp -all -inline -indices {[\[\]]} $text]
    set segments {}

    set i 0
    set substStart -1
    set substEnd -1

    set level 0

    foreach pair $matches {
        set i [lindex $pair 0]
        set bracket [string index $text $i]

        if {$bracket eq $opening} {
            incr level

            if {$level == 1} {
                set substStart $i
            }
        } else {
            set level [expr {
                $level <= 0 ? 0 : $level - 1
            }]
        }

        if {$level != 0} continue

        set candidate [string range $text $substStart $i]
        if {[info complete $candidate]} {
            lappend-nonempty \
                segments \
                [string range $text $substEnd+1 $substStart-1] \
                [string range $text $substStart $i] \

            set substEnd $i
        }
    }

    lappend-nonempty \
        segments \
        [string range $text $substEnd+1 end] \

    return $segments
}


proc tclmark::lappend-nonempty {varName args} {
    upvar 1 $varName list

    foreach el $args {
        if {$el ne {}} {
            lappend list $el
        }
    }
}


proc tclmark::node {tag args} {
    variable token

    list $token $tag $args
}


proc tclmark::flatten node {
    variable token

    if {[llength $node] != 3} {
        error [list not three elements: $node]
    }
    lassign $node nodeToken tag data

    if {$nodeToken ne $token} {
        error [list invalid token in $node]
    }

    switch -- $tag {
        html {
            lindex $data 0
        }
        text {
            entities [lindex $data 0]
        }
        tree {
            join [lmap el $data {
                flatten $el
            }] {}
        }
        default {
            error [list unknown tag: $tag]
        }
    }
}


proc tclmark::entities text {
    string map {
        & &amp;
        < &lt;
        > &gt;
        \" &quot;
        ' &#039;
    } $text
}


foreach tag {b i tt} {
    proc tclmark::cmd::$tag content [format {
        node tree [node html <%1$s>] [msubst $content] [node html </%1$s>]
    } $tag]
}


proc tclmark::generic-list {tag xs} {
    if {$tag ni {ul ol}} {
        error [list invalid tag: $tag]
    }

    node tree \
        [node html <$tag>] \
        {*}[msubst-and-wrap li $xs] \
        [node html </$tag>] \
}


proc tclmark::msubst-and-wrap {tag items} {
    lmap item $items {
        node tree [node html <$tag>] [msubst $item] [node html </$tag>]
    }
}


proc tclmark::cmd::1 args { tailcall generic-list ol $args }
proc tclmark::cmd::* args { tailcall generic-list ul $args }


proc tclmark::generic-heading {n content} {
    if {$n ni {1 2 3 4 5 6}} {
        error [list invalid level: $n]
    }

    node tree \
        [node html <h$n>] \
        [msubst $content] \
        [node html </h$n>] \
}


for {set i 1} {$i <= 6} {incr i} {
    proc tclmark::cmd::h$i content [format {
        tailcall generic-heading %u $content
    } $i]
}


proc tclmark::cmd::link {url {content {}}} {
    if {$content eq {}} {
        set content $url
    }

    node tree \
        [node html "<a href=\"[entities $url]\">"] \
        [msubst $content] \
        [node html </a>] \
}


proc tclmark::cmd::img {src {alt {}}} {
    node html "<img src=\"[entities $src]\" alt=\"$alt\">"
}


proc tclmark::cmd::code content {
    node tree \
        [node html <pre><code>] \
        [node text $content] \
        [node html </code></pre>] \
}


proc tclmark::run-tests {} {
    set stats [dict create total 0 passed 0 failed 0]

    proc test {name script arrow expected} {
        upvar stats stats

        dict incr stats total

        catch $script result

        set matched [switch -- $arrow {
            ->   { expr {$result eq $expected} }
            ->*  { string match $expected $result }
            ->$  { regexp -- $expected $result }
            default {
                return -code error \
                       -errorcode {JIMLIB TEST BAD-ARROW} \
                       [list unknown arrow: $arrow]
            }
        }]

        if {!$matched} {
            set error {}
            append error "\n>>>>> $name failed: [list $script]\n"
            append error "      got: [list $result]\n"
            append error " expected: [list $expected]"
            if {$arrow ne {->}} {
                append error "\n    match: $arrow"
            }

            dict incr stats failed

            puts stderr $error
            return
        }
    }



    set tclmark::debug true


    test segment-1 {
        tclmark::segment {}
    } -> {}

    test segment-2 {
        tclmark::segment a
    } -> a

    test segment-3 {
        tclmark::segment {a b c}
    } -> {{a b c}}

    test segment-4 {
        tclmark::segment {[a]}
    } -> {{[a]}}

    test segment-5 {
        tclmark::segment {[a [b c]]}
    } -> {{[a [b c]]}}

    test segment-6 {
        tclmark::segment {a [b] c}
    } -> {{a } {[b]} { c}}

    test segment-7 {
        tclmark::segment {a [b c] d e}
    } -> {{a } {[b c]} { d e}}

    test segment-8 {
        tclmark::segment {This text has [b two] [i commands].}
    } -> {{This text has } {[b two]} { } {[i commands]} .}

    test segment-9 {
        tclmark::segment {Foo [b {bar [i baz] qux}] quux}
    } -> {{Foo } {[b {bar [i baz] qux}]} { quux}}

    test segment-10 {
        tclmark::segment {[[[a]]]}
    } -> {{[[[a]]]}}


    test basic-1 {
        tclmark::render Test
    } -> Test

    test basic-2 {
        tclmark::render {[b Test]}
    } -> <b>Test</b>

    test basic-3 {
        tclmark::render {[i {Hello, world!}]}
    } -> {<i>Hello, world!</i>}

    test basic-4 {
        tclmark::render {[b [i Test]]}
    } -> <b><i>Test</i></b>

    test basic-5 {
        tclmark::render {[b {[i Test]}]}
    } -> <b><i>Test</i></b>

    test basic-6 {
        tclmark::render {This text has [b two] [i commands].}
    } -> {This text has <b>two</b> <i>commands</i>.}

    test basic-7 {
        tclmark::render {Foo [b {bar [i baz] qux}] quux}
    } -> {Foo <b>bar <i>baz</i> qux</b> quux}


    test html-1 {
        tclmark::render {<b><i>Test</i></b>}
    } -> {&lt;b&gt;&lt;i&gt;Test&lt;/i&gt;&lt;/b&gt;}

    test html-2 {
        tclmark::render {[b <script>alert('Hello!')</script>]}
    } -> {<b>&lt;script&gt;alert(&#039;Hello!&#039;)&lt;/script&gt;</b>}


    test list-1 {
        tclmark::render {[* first second third]}
    } -> {<ul><li>first</li><li>second</li><li>third</li></ul>}

    test list-2 {
        tclmark::render {[1 first second third]}
    } -> {<ol><li>first</li><li>second</li><li>third</li></ol>}


    for {set i 1} {$i <= 6} {incr i} {
        test heading-h$i {
            upvar 1 i i
            tclmark::render "\[h$i {A Heading!}\]"
        } -> "<h$i>A Heading!</h$i>"
    }


    puts stderr $stats

    return [expr {[dict get $stats failed] > 0}]
}


# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    exit [tclmark::run-tests]
}

package provide tclmark 0
